//
//  udp.h
//  hw3
//
//  Created by Arthur Bulan on 5/20/15.
//  Copyright (c) 2015 Arthur Bulan. All rights reserved.
//

#ifndef __hw3__udp__
#define __hw3__udp__

#include <stdio.h>
#include "UdpSocket.h"
#include "Timer.h"
#include <iostream>
#include <stdlib.h>

using namespace std;
class udpa
{
public:
    
    void serverReliable( UdpSocket &sock, const int max, int message[] );
    void serverEarlyRetrans( UdpSocket &sock, const int max, int message[],
                            int windowSize, int loss );
    int clientStopWait( UdpSocket &sock, const int max, int message[] );
    int clientSlidingWindow( UdpSocket &sock, const int max, int message[],
                            int windowSize );
    
};


#endif /* defined(__hw3__udp__) */
