//
//  udp.cpp
//  hw3
//
//  Created by Arthur Bulan on 5/20/15.
//  Copyright (c) 2015 Arthur Bulan. All rights reserved.
//

#include "udp.h"

// Stop and Wait protocol algorithm implementation
int udp::clientStopWait( UdpSocket &sock, const int max, int message[] ) {
    Timer timer;
    int i               = 0;
    long TimeCheck      = 0;
    int retransmit      = 0;
    int ackBack         = 0;
    int timeOut         = 1500;
    
    for (i = 0; i < max;)
    {
        message[0]      = i;                    //set the first element to i
        sock.sendTo((char * ) message, MSGSIZE);    //send the message
        
        TimeCheck       = 0;
        timer.start();                          //timer start
        
        while (TimeCheck < timeOut)             //if its less than timeout
        {
            if ( sock.pollRecvFrom() != 0)      //check is something's there
            {
                sock.recvFrom((char *) message, MSGSIZE);   //receive message
                ackBack = i + 1;
                if ( ackBack == message[0])     //validate ack
                {
                    i++;                        //iterate ack and
                    break;
                }
            }
            
            else
            {
                TimeCheck = timer.lap();
            }
            
            if (TimeCheck >= timeOut)           //if time is more than timeout
            {
                retransmit++;                   //iterate retransmit but not i
            }
            
            cerr << "message = " << message[0] << endl;
            
        }
        
    }
    return retransmit;
}
//Sliding Window protocol algorithm implementation
int udp::clientSlidingWindow( UdpSocket &sock, const int max, int message[],
                        int windowSize ) {
    cerr << "Window: " << windowSize << endl;
    
    Timer timer;
    long TimeCheck              = 0;
    int i                       = 0;
    int minIndex                = 0;
    int window                  = 0;
    int MessagesInTransit       = 0;
    int timeout                 = 1500;
    int retransmissions         = 0;
    while (i < max)
    {
        window = minIndex + windowSize;     //window is set
        
        while (MessagesInTransit < windowSize && i < window && i < max)
        {
            message[0] = i;                 //first element is set to i
            sock.sendTo((char *)message, MSGSIZE);  //message sent
            MessagesInTransit++;
            i++;                            //iterate i
            
            if (sock.pollRecvFrom() != 0) { //check is there's something in buffer
                sock.recvFrom((char *)message, MSGSIZE);    //receive the message
                minIndex = message[0];
                MessagesInTransit--;
            }
            window = minIndex + windowSize;
        }
        
        if ( i < max)                       // if i is less than max
        {
            TimeCheck = 0;
            timer.start();                  //timer started
            
            while (TimeCheck < timeout) {   //if time is less than timeout
                if (sock.pollRecvFrom() != 0) { //check if there something in buffer
                    sock.recvFrom((char *)message, MSGSIZE);    //receive
                    minIndex = message[0];
                    MessagesInTransit--;        //decrement because it was sent
                    break;
                }
                TimeCheck = timer.lap();        //lap the timer
            }
            if (TimeCheck > timeout) {          // if more than timeout
                message[0] = minIndex;
                sock.sendTo((char *) message, MSGSIZE); //resend
                retransmissions++;              //iterate
            }
        }
    }
    
    return retransmissions;
}
// Server for Stop and Wait
void udp::serverReliable( UdpSocket &sock, const int max, int message[] ) {
    cerr << "server reliable test:" << endl;
    
    int index = -1;
    int i = 0;
    
    while ( i < max )
    {
        sock.recvFrom( ( char * ) message, MSGSIZE );   // udp message receive
        if (index < message[0])
        {
            index = message[0];                         //set index to sequence num
            i++;
        }
        
        message[0]++;
        
        sock.ackTo((char*)message, MSGSIZE);           //send Ack
    }
}


void udp::serverEarlyRetrans( UdpSocket &sock, const int max, int message[],
                        int windowSize ) {
    
    cout << "Server Sliding Windows: " << endl;
    bool * array = new bool[max];           //create an array of bools
    for (int i = 0; i < max; i++)               //set all elements to false
    {
        array[i] = false;
    }
    
    int min;
    int cur;
    
    for (int i = 0; i < max;)
    {
        sock.recvFrom((char* )message, MSGSIZE);    //receive message
        cur = 0;
        cur = message[0];
        
        if (array[cur] == false)
        {
            i++;
            array[cur] = true;               //set elemtnts to true
        }
        min = 0;
        for (int i = min; i < max; i++)
        {
            if (array[i] == false)
            {
                min  = i;                  //set minIndex to index
                break;
            }
        }
        
        message[0] = min;
        
        sock.ackTo((char *)message, MSGSIZE);       // send an ack
    }
    
    delete[] array;
}

